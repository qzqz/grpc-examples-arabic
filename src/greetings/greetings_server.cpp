#include <iostream>
#include <memory>
#include <string>

// مكتبة جربسي
#include <grpc++/grpc++.h>

// رأسية تنتج من ترجمة ملف بروتو
#include "greetings.grpc.pb.h"

// استخدامات الخادم
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;

// استخدامات مشتركة بين العميل و الخادم
using grpc::Status;

// استخدامات من رأسية بروتو
using greetings::Greeter;
using greetings::HelloReply;
using greetings::HelloRequest;

// ------------------------------------------------------------

/* المنطق والبيانات لسلوك الخادم
   ببساطة إعادة كتابة مهام الدالة
    من صنف greeting
    الذي تم توليده من ملف بروتو

    اسم الدالة في الخدمة من ملف بروتو
    المعطيات و المخرجات للدالة من ملف بروتو ايضا

    المعطيات و المخرجات تدعى رسائل

*/

class GreeterServiceImpl final : public Greeter::Service {
  Status SayHello(ServerContext *context, const HelloRequest *request,
                  HelloReply *reply) override {
    std::string prefix("Hello ");
    reply->set_message(prefix + request->name() + "!");
    return Status::OK;
  }
};

/*
    كما فهمت فمتغيرات ملف بروتو محايدة ليست مقيدة من جهة الخادم او العميل
    لذلك من الأفضل وضع اسماء واضحة

    للتعديل يتم كتابة
    set_
    قبل الرسالة (الرسالة تحتوي متغيرات)

    في النهاية يرجع الحالة ، حالة الإتصال

*/

// ------------------------------------------------------------

void RunServer() {
  std::string server_address("0.0.0.0:50051");

  // انشاء مثيل للخدمة (instance)
  GreeterServiceImpl service;

  // إنشاء مثيل لبناء الخادم
  ServerBuilder builder;

  // فتح قناة استماع بدون آلية مصادقة
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  // تسجيل "الخدمة" كمثيل/كائن تتواصل من خلاله مع العملاء
  // هذه الحالة تتوافق مع خدم (متزامنة)
  // اظن هذه العملية من تعطي كل اتصال جديد خيط جديد للمعالجة
  builder.RegisterService(&service);

  // أخيرًا تجميع الخادم
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // انتظار حتى إيفاق تشغيل الخادم
  // كما فهمت فهذه تعلق هذا الخيط حيط لا يخرج الخادم عن التشغيل
  // تشغيل الخادم يتم في الخيط الأساسي لذلك يعلق و الطلبات ترد على خطوط اخرى
  // في حالة وضعية التزامن(مهام متزامنة)
  server->Wait();
}

// ------------------------------------------------------------

int main(int argc, char **argv) {
  RunServer();
  return 0;
}
