#include <iostream>
#include <memory>
#include <string>

// مكتبة جربسي
#include <grpc++/grpc++.h>

// رأسية ناتجة من ترجمة ملف بروتو
#include "greetings.grpc.pb.h"

// استخدامات العميل 
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

// استخدامات من رأسية بروتو
// الخدمة و رسالة المعطى و رسالة المرجع(المخرج)
using greetings::Greeter;
using greetings::HelloReply;
using greetings::HelloRequest;


// ------------------------------------------------------------

class GreeterClient {
public:
    // باني الخادم
    GreeterClient(std::shared_ptr<Channel> channel): stub_(Greeter::NewStub(channel)) {}

    // ملئ رسالة المعطى و طلب الرد(المرجع) و حفظ الحالة للإتصال
    std::string SayHello(const std::string & user) {
   
        // البيانات المرسلة للخادم (معطيات) 
        HelloRequest request;
        request.set_name(user);

        // رسالة فارغة للبيانات المتوقعة في الرد من الخادم 
        HelloReply reply;

        // Context for the client. 
        // It could be used to convey extra information to the server and/or tweak certain RPC behaviors.
        
        // إطار العميل
        //يمكن استخدامه لنقل معلومات إضافية إلى الخادم و/أو تعديل بعض سلوكيات RPC.
        ClientContext context;

        // الإتصال الفعلي
        Status status = stub_->SayHello(&context, request, &reply);

        // التصرف بحسب حالة الخادم
        if (status.ok()) {
            return reply.message();
        } 
        else {
            std::cout << status.error_code() << ": " << status.error_message() << std::endl;
            return "gRPC failed";
        }
    }

private:
    // يتم ملؤه من خلال باني الصنف
    std::unique_ptr<Greeter::Stub> stub_;
};

void InterativeGRPC() {
    
    //توصيل العميل. يتطلب قناة، يتم إنشاء ار بي سي الفعلية منها.
    // نقطة اتصال  localhost في المنفذ 50051
    //نشير إلى أن القناة غير موثقة InsecureChannel
    GreeterClient greeter(grpc::CreateChannel(
      "localhost:50051", grpc::InsecureChannelCredentials()));
  while (true) {
    std::string user;
    std::cout << "Please enter your user name:" << std::endl;
    // std::cin >> user;
    std::getline(std::cin, user);
    std::string reply = greeter.SayHello(user);
    if (reply == "gRPC failed") {
      std::cout << "gRPC failed" << std::endl;
    }
    std::cout << "gRPC returned: " << std::endl;
    std::cout << reply << std::endl;
  }
}

int main() {

  InterativeGRPC();

  return 0;
}

